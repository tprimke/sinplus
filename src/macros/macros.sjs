macro req {
  rule {
    $ident $path
  } => {
    var $ident = ( function() {
      var sinplus = require('sinplus');
      return sinplus.require( require, $path );
    })();
  }
}

macro test_req {
  rule {
    $ident $path
  } => {
    var $ident = ( function() {
      var sinplus = require('../sinplus');
      return sinplus.require( require, $path );
    })();
  }
}

export req;
export test_req;

