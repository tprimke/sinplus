var b = (function() {
  var sinonplus = require('../sinplus');
  return sinonplus.require(require, './b');
})();

exports.funcA = function() {
  return b.funcB();
};

