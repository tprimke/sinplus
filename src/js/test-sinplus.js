var should = require('should');

var defined = function( stub ) {
  (stub !== undefined).should.be.true;
  (stub !== null).should.be.true;
};

var checkStub = function( stub, val ) {
  defined( stub );
  stub.should.be.a.Function;
  (stub()).should.eql(val);
  stub.should.have.property('called');
  (stub.called).should.be.true;
};

describe( 'Make stubs', function() {

  var stubs = require('./sinplus');

  var checkNumber = function( stub, num ) {
    defined( stub );
    stub.should.be.a.Number;
    stub.should.eql(num);
  };

  var checkString = function( stub, st ) {
    defined( stub );
    stub.should.be.a.String;
    stub.should.eql(st);
  };

  var zero = function() { return 0; };

  it('should provide a spy for a function', function() {
    var the_zero = function() { return 0; };

    var spy = stubs.getStub( the_zero );
    defined( spy );
    spy.should.be.a.Function;
    (spy.called).should.be.false;
    checkStub( spy, 0 );
  });
  
  it('should provide a stub for a function', function() {
    var stub = stubs.getStub( zero, 1 );

    defined( stub );
    stub.should.be.a.Function;
    (stub.called !== true).should.be.true;
    (stub()).should.eql(1);
    (stub.called).should.be.true;
    (stub.calledOnce).should.be.true;
  });

  it('should provide a configured stub for a function', function() {
    var stub = stubs.getStub( zero, function(stub) {
      stub.withArgs(10).returns(10);
      stub.returns(5);
    });

    defined(stub);
    stub.should.be.a.Function;
    (stub.called !== true).should.be.true;
    (stub()).should.eql(5);
    (stub(10)).should.eql(10);
  });

  it('should not provide a stub for primitive values', function() {
    var stub = stubs.getStub( 1, 10 );

    checkNumber( stub, 1 );
    
    stub = stubs.getStub( "1", 10 );
    checkString( stub, '1' );
    
    stub = stubs.getStub( false, 10 );
    defined( stub );
    stub.should.be.a.Boolean;
    stub.should.be.false;

    stub = stubs.getStub( null, 10 );
    (stub === null).should.be.true;

    stub = stubs.getStub( undefined, 10 );
    (stub === undefined).should.be.true;
  });

  it('use sinon stubs', function() {
    var stub = stubs.sinon.stub();
    stub.withArgs(1).returns(1);
    stub.returns(2);
    
    (stub()).should.eql(2);
    (stub(1)).should.eql(1);
  });

  describe( 'arrays', function() {
    
    it('flat arrays', function() {
      var arr = [ 1, '1', zero, 1 ];

      var stub_arr = stubs.getStub( arr, [1] );
      defined( stub_arr );
      stub_arr.should.be.an.Array;

      checkNumber( stub_arr[0], 1 );
      checkString( stub_arr[1], '1' );
      checkStub( stub_arr[2], 1 );
      checkNumber( stub_arr[3], 1 );
    });

    it('flat arrays with the proper number of stub values', function() {
      var arr = [ zero, zero, zero ];
      var stub_arr = stubs.getStub( arr, [1, 2, 3] );
    
      checkStub( stub_arr[0], 1 );
      checkStub( stub_arr[1], 2 );
      checkStub( stub_arr[2], 3 );
    });

    it('cyclic values', function() {
      var arr = [ zero, zero, zero ];
      var stub_arr = stubs.getStub( arr, 1 );
      
      checkStub( stub_arr[0], 1 );
      checkStub( stub_arr[1], 1 );
      checkStub( stub_arr[2], 1 );
    });

    it('nested arrays', function() {
      var arr = [zero, [zero, [zero, 1], 1], 1];
      var stub_arr = stubs.getStub( arr, [1, [2, [3]]] );

      checkStub( stub_arr[0], 1 );
      checkStub( stub_arr[1][0], 2 );
      checkNumber( stub_arr[1][2], 1 );
      checkStub( stub_arr[1][1][0], 3 );
      checkNumber( stub_arr[1][1][1], 1 );
    });

  }); // arrays

  describe( 'objects', function() {
    
    it('flat object', function() {
      var obj = {
        doSomething: zero,
        doSomethingElse: zero,
        justOne: 1
      };

      var stub = stubs.getStub( obj, { doSomething: 1 } );
      defined( stub );
      stub.should.be.an.Object;

      stub.should.have.property('doSomething');
      checkStub( stub.doSomething, 1 );

      stub.should.have.property('doSomethingElse');
      stub.doSomethingElse.should.be.a.Function;
      (stub.doSomethingElse()).should.eql(0);
      (stub.doSomethingElse).should.not.have.property('called');

      stub.should.have.property('justOne', 1);
    });

    it('nested objects', function() {
      var obj = {
        other: {
          doSomething: zero,
          third: {
            doSomething: zero
          }
        }
      };

      var stub = stubs.getStub( obj, {
        other: {
          doSomething: 10,
          third: {
            doSomething: 20
          }
        }
      });

      defined( stub );
      stub.should.be.an.Object;

      stub.should.have.property('other');

      stub.other.should.have.property('doSomething');
      checkStub( stub.other.doSomething, 10 );

      stub.other.should.have.property('third');

      stub.other.third.should.have.property('doSomething');
      checkStub( stub.other.third.doSomething, 20 );
    });

  }); // objects

}); // Make stubs


describe( 'configure and require', function() {

  var stubs;

  beforeEach( function() {
    stubs = require('./sinplus');
  });

  afterEach( function() {
    stubs.reset();
    stubs = undefined;
  });

  it('reset the module', function() {
    (stubs.configure('env') === undefined).should.be.true;
    (stubs.configure('no-stubs').length).should.eql(0);
    stubs.reset();
    (stubs.configure('env') === undefined).should.be.true;
    (stubs.configure('no-stubs').length).should.eql(0);
    stubs.configure('env', 'test');
    stubs.configure('no-stubs', 'alfa');
    (stubs.configure('env') === 'test').should.be.true;
    (stubs.configure('no-stubs').length).should.eql(1);
    stubs.reset();
    (stubs.configure('env') === undefined).should.be.true;
    (stubs.configure('no-stubs').length).should.eql(0);
  });
  
  it('manage the configuration object', function() {
    (stubs.configure('env') === undefined).should.be.true;
    stubs.configure('env', 'prod');
    (stubs.configure('env')).should.eql('prod');
  });

  it('the require function', function() {
    stubs.configure('require', {
      sinplus: {
        configure: 10,
        require: 20,
        getStub: 30
      }
    });
    var new_stubs = stubs.require(require, './sinplus');

    defined( new_stubs );
    new_stubs.should.be.an.Object;

    new_stubs.should.have.property('configure');
    checkStub( new_stubs.configure, 10 );

    new_stubs.should.have.property('require');
    checkStub( new_stubs.require, 20 );

    new_stubs.should.have.property('getStub');
    checkStub( new_stubs.getStub, 30 );
  });

  it('changing stubs in consequent calls', function() {
    stubs.configure('require', {
      b: {
        funcB: 20
      }
    });
    stubs.configure('no-stubs', 'a');

    var a = stubs.require( require, './test/a' );

    defined( a );
    a.should.be.an.Object;

    a.should.have.property('funcA');
    (a.funcA).should.be.a.Function;
    (a.funcA()).should.eql(20);

    stubs.configure('require', {
      b: {
        funcB: 200
      }
    });

    var a = stubs.require( require, './test/a' );

    defined( a );
    a.should.be.an.Object;

    a.should.have.property('funcA');
    (a.funcA).should.be.a.Function;
    (a.funcA()).should.eql(200);
  });

  it('the real require', function() {
    stubs.configure('no-stubs', 'sinplus' );

    var new_stubs = stubs.require(require, './sinplus');
    
    (new_stubs.configure).should.not.have.property('callCount');
    (new_stubs.require).should.not.have.property('callCount');
    (new_stubs.getStub).should.not.have.property('callCount');
  });

  it('configure no-stubs by subsequent calls', function() {
    stubs.configure('no-stubs', 'alpha' );
    stubs.configure('no-stubs', 'beta' );
    (stubs.configure('no-stubs')).should.be.an.Array;
    var arr = stubs.configure('no-stubs');
    'alpha'.should.match(arr[0]);
    'beta'.should.match(arr[1]);
  });

  it('configure no-stubs by arrays', function() {
    stubs.configure('no-stubs', ['alpha', 'beta']);
    stubs.configure('no-stubs', ['gamma', 'delta']);
    (stubs.configure('no-stubs')).should.be.an.Array;
    var arr = stubs.configure('no-stubs');
    'alpha'.should.match(arr[0]);
    'beta'.should.match(arr[1]);
    'gamma'.should.match(arr[2]);
    'delta'.should.match(arr[3]);
  });
  
  it('require in other directory', function() {
    stubs.configure('require', {
      b: {
        funcB: 10
      }
    });
    stubs.configure('no-stubs', 'a');

    var a = stubs.require( require, './test/a' );

    defined( a );
    a.should.be.an.Object;

    a.should.have.property('funcA');
    (a.funcA).should.be.a.Function;
    (a.funcA()).should.eql(10);
  });

  it('req macro', function() {
    stubs.configure('require', {
      b: {
        funcB: 10
      }
    });
    stubs.configure('no-stubs', 'a-req');

    var a = stubs.require( require, './test/a-req' );

    defined( a );
    a.should.be.an.Object;

    a.should.have.property('funcA');
    (a.funcA).should.be.a.Function;
    (a.funcA()).should.eql(10);
  });
}); // require

