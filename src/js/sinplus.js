var _ = require('lodash');

var sinon = require('sinon');

exports.sinon = sinon;


// Returns true, if the given parameter is a primitive value.
var isPrimitive = function( val ) {
  return ( (typeof val === 'number') ||
           (typeof val === 'string') ||
           (typeof val === 'boolean') ||
           (typeof val === 'undefined') ||
           (val === null) );
};

// Returns a stub for the given function.
// If the value parameter is given, the returned stub will return the
// value on call.
// When the given value is a function, the function is called, and the
// created stub is passed to it.
// If the value is not given, a sinon spy will be returned.
var getFunctionStub = function( func, value ) {
  var stub;

  if ( value === undefined ) {
    return sinon.spy( func );
  }
  else {
    stub = sinon.stub();
    if ( typeof value === 'function' )
      value( stub );
    else
      stub.returns( value );
    return stub;
  }
};

// For the given array of objects, returns an array of stubs.
// When an array of stub values is provided, they are used one by one.
// When only one value is given, it is used for all the stubs.
var getArrayStub = function( arr, values ) {
  var stubs = [], stub;
  _.forEach( arr, function( val ) {

    // if many values have been given, get the next one
    if ( _.isArray( values ) )
      stub = getStubOrNothing( val, values[0] );
    // otherwise, use the given value for all the stubs
    else
      stub = getStubOrNothing( val, values );

    // if no stub has been created, simply use the value from the
    // array
    if ( stub === null )
      stubs.push( val );
    // otherwise, use the stub and update the stubs values array, if
    // needed
    else {
      stubs.push( stub );
      if ( _.isArray( values ) )
        values.shift();
    }
  });

  // here, values should be empty
  // although it's not neccessary
  // the remaining values are the unused ones, though
  return stubs;
};

// For the given object, returns a stub.
var getObjectStub = function( obj, a_stub ) {
  
  var stub = {};
  _.forOwn( obj, function( num, key ) {
    if ( a_stub.hasOwnProperty(key) ) {
      stub[key] = getStub( obj[key], a_stub[key] );
    }
    else {
      stub[key] = obj[key];
    }
  });

  return stub;
};

// If the given something is stubbable, returns a stub.
// Otherwise, returns null.
var getStubOrNothing = function( sthg, stub_values ) {
  if ( typeof sthg === 'function' ) {
    return getFunctionStub( sthg, stub_values );
  }

  if ( isPrimitive( sthg ) ) {
    return null;
  }

  if ( _.isArray( sthg ) ) {
    return getArrayStub( sthg, stub_values );
  }

  return getObjectStub( sthg, stub_values );
};

// Returns a stub for the given something.
var getStub = function( sthg, a_stub ) {
  var stub = getStubOrNothing( sthg, a_stub );
  if ( stub === null )
    return sthg;
  return stub;
};

exports.getStub = getStub;


// ------------------------------------------------------------------
// Here starts the configuration / require part.

// The configuration object.
// It's something like a "global" namespace to use.
var conf = {
  // the modules, which shouldn't be stubbed
  'no-stubs': []
};

// Sets / returns a configuration objects.
//
// When both the name and value are passed, the name kay in the
// configuration object is set to the value.
//
// When the value is not given, the object of the passed key is
// returned from the configuration object.
//
// This is one special case for setting the configuration object: the
// no-stubs key. The key is supposed to be an array of regular
// expressions, which are then matched against the paths passed to the
// require method.
var configure = function( name, val ) {
  if ( val === undefined )
    return conf[name];

  if ( name === 'no-stubs' ) {
    // when an array of names have been given, process the names one
    // by one
    if ( _.isArray(val) ) {
      _.forEach( val, function(v) {
        configure( 'no-stubs', v );
      });
    }
    // otherwise simply add a new regular expression to the array
    else {
      conf['no-stubs'].push( new RegExp( val + '$' ) );
    }
  }
  else
    conf[name] = val;
};

exports.configure = configure;

// save the real require function for use in the require function from
// this module
//var real_require = require;

// Loads the requested module, using the real, node.js require
// function.
//
// If the module is not supposed to be stubbed, returns the loaded
// module.
//
// Otherwise, a stub for the module is created, and the stub is
// returned.
var require = function( real_require, path ) {
  // get the real module
  var the_module = real_require( path ),
      the_stub;
  
  // if there are some modules, which shouldn't be stubbed...
  if ( conf.hasOwnProperty('no-stubs') ) {
    if ( _.any( conf['no-stubs'], function( re ) {
           return (path.match(re) !== null);
         }) )
      return the_module;
  }

  // check the configured modules
  if ( conf.hasOwnProperty('require') ) {
    _.forOwn( conf.require, function( num, key ) {
      // if the stub has been found, there's nothing to do anymore
      if ( the_stub !== undefined )
        return;

      // if the module's path doesn't match the configured key, we
      // have nothing to do
      if ( path.match( new RegExp( key + '$' ) ) === null )
        return;

      // that's the module - get a stub for it
      the_stub = getStub( the_module, conf.require[key] );
    });

    return the_stub;
  }
};

exports.require = require;

// Resets the module's state.
var reset = function() {
  conf = {
    'no-stubs': []
  };
};

exports.reset = reset;
