var gulp    = require('gulp'),
    clean   = require('gulp-clean'),
    concat  = require('gulp-concat'),
    mocha   = require('gulp-mocha'),
    runSeq  = require('run-sequence'),
    sweetjs = require('gulp-sweetjs');

gulp.task('clean', function() {
  return gulp.src('build', {read: false})
             .pipe( clean() );
});

gulp.task('build-test-cli-macro', function() {
  return gulp.src('src/js/**/test/*.js')
             .pipe( sweetjs({
               modules: ['./src/macros/macros.sjs']
             }))
             .pipe( gulp.dest('build') );
});

gulp.task('build-test-cli-test', function() {
  return gulp.src('src/js/*.js')
             .pipe( gulp.dest('build') )
});

gulp.task('test-cli-run', function() {
  return gulp.src('build/**/test-*.js', {read: false})
             .pipe( mocha() );
});

gulp.task('test-cli', function() {
  return runSeq(
    'clean',
    ['build-test-cli-macro', 'build-test-cli-test'],
    'test-cli-run'
  );
});

